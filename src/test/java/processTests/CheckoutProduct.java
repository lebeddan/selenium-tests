package processTests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.LoginPage;

import java.io.IOException;
import java.util.List;

public class CheckoutProduct
{
    private WebDriver driver;

    @BeforeEach
    void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "/Users/Darya/Desktop/chromedriver");
        driver = new ChromeDriver();
    }

    void openPage() throws InterruptedException
    {
        driver.get("https://www.saucedemo.com");
        Thread.sleep(1000);
    }

    void login(String username, String password) throws InterruptedException
    {
        WebElement userNameField = driver.findElement(By.id("user-name"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement submitButton = driver.findElement(By.id("login-button"));

        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    @Test
    void buyProductTest() throws InterruptedException
    {
        String username = "standard_user";
        String password = "secret_sauce";
        String exeptedPage = "https://www.saucedemo.com/checkout-complete.html";

        openPage();
        login(username, password);

        WebElement addBackpackButton = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement cartButton = driver.findElement(By.id("shopping_cart_container"));

        addBackpackButton.click();
        Thread.sleep(1000);
        cartButton.click();
        Thread.sleep(1000);

        WebElement checkoutButton = driver.findElement(By.id("checkout"));
        checkoutButton.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daniil");
        surnameField.sendKeys("Lebedev");
        zipCodeField.sendKeys("166 00");

        continueButton.click();
        Thread.sleep(1000);

        WebElement finishButton = driver.findElement(By.id("finish"));
        finishButton.click();
        Assertions.assertEquals(exeptedPage, driver.getCurrentUrl());
    }

    @Test
    void imageProductTest() throws InterruptedException, IOException
    {
        String username = "problem_user";
        //String username = "standard_user";

        String password = "secret_sauce";
        String exeptedImage = "https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.34e7aa42.jpg";

        openPage();
        login(username, password);

        WebElement productImage = driver.findElement(By.xpath("//img[@alt='Sauce Labs Backpack']"));
        Assertions.assertEquals(exeptedImage, productImage.getAttribute("src"));
    }

    @Test
    void lowToHighFilterTest() throws InterruptedException
    {
        String username = "standard_user";
        String password = "secret_sauce";
        boolean correctOrderCheck = false;
        double maxPrice = 49.99;

        openPage();
        login(username, password);
        WebElement sortButton = driver.findElement(By.className("product_sort_container"));
        sortButton.click();
        sortButton.findElement(By.xpath("//select/option[@value='lohi']")).click();// low to high
        // Get items prices
        List<WebElement> lowestPriceProduct = driver.findElements(By.xpath("//div[@class='inventory_item_price']"));
        String lowestPrice = lowestPriceProduct.get(0).getText().substring(1);

        for (WebElement element : lowestPriceProduct)
        {
            String price = element.getText().substring(1);
            // If order is not correct, change flag
            if (Double.parseDouble(price) < Double.parseDouble(lowestPrice))
                correctOrderCheck = true;
            else if (Double.parseDouble(price) > maxPrice)
                correctOrderCheck = true;
        }

        Assertions.assertFalse(correctOrderCheck);
        WebElement addLowestProductToCart = driver.findElement(By.id("add-to-cart-sauce-labs-onesie"));
        WebElement cartButton = driver.findElement(By.id("shopping_cart_container"));

        addLowestProductToCart.click();
        Thread.sleep(1000);
        cartButton.click();
        Thread.sleep(1000);

        WebElement checkoutButton = driver.findElement(By.id("checkout"));
        checkoutButton.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daniil");
        surnameField.sendKeys("Lebedev");
        zipCodeField.sendKeys("166 00");

        continueButton.click();
        Thread.sleep(1000);

        WebElement finishButton = driver.findElement(By.id("finish"));
        finishButton.click();
    }

    @Test
    void infoFieldsTest() throws InterruptedException
    {
        String username = "problem_user";
        String password = "secret_sauce";
        String exeptedPage = "https://www.saucedemo.com/checkout-step-two.html";

        openPage();
        login(username, password);

        WebElement addBackpackButton = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement cartButton = driver.findElement(By.id("shopping_cart_container"));

        addBackpackButton.click();
        Thread.sleep(1000);
        cartButton.click();
        Thread.sleep(1000);

        WebElement checkoutButton = driver.findElement(By.id("checkout"));
        checkoutButton.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daniil");
        surnameField.sendKeys("Lebedev");
        zipCodeField.sendKeys("166 00");

        continueButton.click();
        Thread.sleep(1000);
        Assertions.assertEquals(exeptedPage, driver.getCurrentUrl());

        WebElement finishButton = driver.findElement(By.id("finish"));
        finishButton.click();
    }

    @Test
    public void tryToBuyLikeProblemUser() throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        WebDriver driver = loginPage.getDriver();
        loginPage.loginProblemUser();

        WebElement addBackpackButton = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement cartButton = driver.findElement(By.id("shopping_cart_container"));

        addBackpackButton.click();
        Thread.sleep(1000);
        cartButton.click();
        Thread.sleep(1000);

        WebElement checkoutButton = driver.findElement(By.id("checkout"));
        checkoutButton.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daria");
        surnameField.sendKeys("Antonova");
        zipCodeField.sendKeys("164 00");

        continueButton.click();
        Assertions.assertNotEquals(surnameField.getAttribute("value"),"Antonova");
    }

    @AfterEach
    public void clean() {
        driver.quit();
    }


}
