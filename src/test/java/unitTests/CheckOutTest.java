package unitTests;


import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;


public class CheckOutTest
{
    private WebDriver driver;

    @BeforeEach
    void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "/Users/SamSeppi/Desktop/chromedriver");
        driver = new ChromeDriver();
    }

    void openPage() throws InterruptedException
    {
        driver.get("https://www.saucedemo.com");
        Thread.sleep(1000);
    }

    void login(String username, String password) throws InterruptedException
    {
        WebElement userNameField = driver.findElement(By.id("user-name"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement submitButton = driver.findElement(By.id("login-button"));

        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    @Test
    void checkoutInfoTest() throws InterruptedException
    {
        String user = "standard_user";
        String password = "secret_sauce";
        List<String> expectedItems = new ArrayList();
        List<String> actualItems = new ArrayList();
        expectedItems.add("Sauce Labs Backpack");
        expectedItems.add("Sauce Labs Bike Light");

        openPage();
        login(user, password);

        WebElement addItem1 = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement addItem2 = driver.findElement(By.id("add-to-cart-sauce-labs-bike-light"));
        WebElement cart = driver.findElement(By.id("shopping_cart_container"));

        addItem1.click();
        Thread.sleep(1000);
        addItem2.click();
        Thread.sleep(1000);
        cart.click();
        Thread.sleep(1000);
        WebElement checkout = driver.findElement(By.id("checkout"));

        checkout.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daniil");
        surnameField.sendKeys("Lebedev");
        zipCodeField.sendKeys("166 00");
        continueButton.click();

        List<WebElement> checkoutItems = driver.findElements(By.className("inventory_item_name"));

        for (WebElement element : checkoutItems)
            actualItems.add(element.getText());

        Assertions.assertLinesMatch(expectedItems, actualItems);
    }

    @Test
    void infoFieldsTest() throws InterruptedException
    {
        String user = "standard_user";
        String password = "secret_sauce";
        String expectedURL = "https://www.saucedemo.com/checkout-step-two.html";

        openPage();
        login(user, password);

        WebElement addItem1 = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement addItem2 = driver.findElement(By.id("add-to-cart-sauce-labs-bike-light"));
        WebElement cart = driver.findElement(By.id("shopping_cart_container"));

        addItem1.click();
        Thread.sleep(1000);
        addItem2.click();
        Thread.sleep(1000);
        cart.click();
        Thread.sleep(1000);
        WebElement checkout = driver.findElement(By.id("checkout"));

        checkout.click();
        Thread.sleep(1000);

        WebElement nameField = driver.findElement(By.id("first-name"));
        WebElement surnameField = driver.findElement(By.id("last-name"));
        WebElement zipCodeField = driver.findElement(By.id("postal-code"));
        WebElement continueButton = driver.findElement(By.id("continue"));

        nameField.sendKeys("Daniil");
        surnameField.sendKeys("Lebedev");
        zipCodeField.sendKeys("166 00");
        continueButton.click();

        Assertions.assertEquals(expectedURL, driver.getCurrentUrl());
    }

    @Test
    void showInfoAboutItemTest() throws InterruptedException
    {
        String user = "standard_user";
        String password = "secret_sauce";
        String expectedURL = "https://www.saucedemo.com/inventory-item.html?id=5";

        openPage();
        login(user, password);

        WebElement item1 = driver.findElement(By.id("item_5_img_link"));
        item1.click();

        Assertions.assertEquals(expectedURL, driver.getCurrentUrl());

    }

}
